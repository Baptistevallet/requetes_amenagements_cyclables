(	-- Soit
	(	-- une route à double sens
		(
			"oneway" IS NULL
			OR
			"oneway" LIKE 'no'
		)
		AND
		(
			"junction" IS NULL
			OR
			"junction" NOT IN ('roundabout','circular')
		)
	)
	AND --et qui a
	(	-- soit une bande cyclable classique
		"cycleway" = 'shoulder'
		OR
		(	-- soit une bande cyclable à droite et à gauche quelque soit le sens
			"cycleway:right" = 'shoulder'
			AND "cycleway:left" = 'shoulder'
		)
		OR	-- soit une bande cyclable à droite et à gauche quelque soit le sens
		"cycleway:both" = 'shoulder'
	)
)

