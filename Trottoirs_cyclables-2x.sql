( --modélisation avec un way séparé
    ( "segregated" IS NULL OR "segregated" NOT IN  ('yes'))
    AND
    "highway" LIKE 'footway'
    AND
    "footway" LIKE 'sidewalk'
    AND
    "bicycle" IN ('yes', 'designated', 'official')
    AND
    (
        (
            "oneway" IS NULL
            OR
            "oneway" LIKE 'no'
        )
        AND
        (
            "oneway:bicycle" IS NULL
            OR
            "oneway:bicycle" LIKE 'no'
        )
    )
)
OR
(  --modélisation sur la voirie

    "sidewalk" = 'both'
    AND
    (
        "sidewalk:both:bicycle" IN ('yes', 'designated', 'official')
        OR
        (
           "sidewalk:right:bicycle" IN ('yes', 'designated', 'official')
           AND
           "sidewalk:left:bicycle" IN ('yes', 'designated', 'official')
        )
    )
    AND
    (
        (
            "sidewalk:segregated" = 'no'
            OR
            "sidewalk:segregated" IS NULL
        )
        AND
        (
            "sidewalk:both:segregated" = 'no'
            OR
            "sidewalk:both:segregated" IS NULL
        )
    )
)
