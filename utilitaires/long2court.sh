#! /bin/bash  

#utilisation ./long2court.sh dossier_a compresser


rm -r compresse

mkdir compresse

cp *.sql compresse/

cd compresse


find -name "*.sql" -print |while read fichier
do
   
sed -i -e '/--/{ N; s/--.*\n/\n/ }' "$fichier" # Supprime les commentaires 1x
sed -i -e '/--/{ N; s/--.*\n/\n/ }' "$fichier"  # Supprime les commentaires 2x
sed -i 's/\t/ /g' "$fichier"  # Remplace les tabulations
sed -i '/^\s*$/d' "$fichier"  # Supprime les lignes vides
sed -i ':a;N;$!ba;s/\n/ /g' "$fichier"  # Remplace les sauts de ligne par une espace (sauf la dernière)
sed -i 's/  */ /g' "$fichier"  # Remplace les espaces multiples par une espace


done

