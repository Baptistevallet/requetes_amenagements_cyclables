(
	"oneway" IN ('yes','-1')
	OR
	"oneway:bicycle" IN ('yes','-1')
)
AND
(
	(
		"highway" = 'path'
		AND
		"bicycle" = 'designated'
		AND
		"foot" = 'designated'
	)
)
