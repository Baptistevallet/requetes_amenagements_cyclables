(
	( "oneway" IS NULL OR "oneway" NOT IN ('yes','-1'))
	AND
	( "oneway:bicycle" IS NULL OR "oneway" NOT IN ('yes','-1'))
)
AND
(
	( --cas des footway autorisé aux vélo mais pas (designated) on ne prend pas les trottoirs
		( "highway" = 'footway' AND ( "footway" IS NULL OR "footway" NOT IN ('sidewalk')))
		AND
		"bicycle" IN ('yes','destination')
	)
	OR
	( --cas des path qui ne sont ni des voies verte et ou le vélo n'est pas (designated) ou interdit
		"highway" = 'path'
		AND
		"bicycle" IN ('yes','destination')
	)
)
AND
(   -- Elles ont un revetement circulable au VTC vélo de randonnée ou le revetement n'est pas connu
    (
        (
            "surface" IN ('paved','asphalt','concrete','concrete:plates','concrete:lanes','paving_stones','sett','unhewn_cobblestone','cobblestone','metal','wood','unpaved','compacted','fine_gravel','gravel','pebblestone','ground','tartan','clay','metal_grid' )
            OR
            "surface" IS NULL
        )
        AND
        (
            "smoothness" NOT IN ('bad','very_bad','horrible','very_horrible','impassable')
            OR
            "smoothness" IS NULL
        )
        AND
        (
            "tracktype" NOT iN ('grade3','grade4','grade5')
            OR
            "tracktype" IS NULL
        )
    )
)