
(
    "cyclestreet" = 'yes'
    OR
    "bicycle_road" = 'yes'
)

AND "oneway" IN ('yes','-1')

AND
(
	"bicycle" IS NULL
	OR
	"bicycle" NOT IN ('no')
)