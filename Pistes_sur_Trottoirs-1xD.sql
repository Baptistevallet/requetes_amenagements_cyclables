
( --modélisation avec un way séparé
    "segregated" LIKE 'yes'
    AND
    "highway" LIKE 'footway'
    AND
    "footway" LIKE 'sidewalk'
    AND
    "bicycle" IN ('yes', 'designated', 'official')
    AND
    (
        "oneway" = 'yes'
        OR
        "oneway:bicycle" = 'yes'
    )
)
OR
(  --modélisation sur la voirie
    (
        "sidewalk" = 'right'
        OR
        "sidewalk" = 'both'
    )
    AND
    (
        "sidewalk:right:bicycle" IN ('yes', 'designated', 'official')
        AND
        (
           "sidewalk:left:bicycle" IS NULL
           OR
           "sidewalk:left:bicycle" = 'no'
        )
    )
    AND
    (
        "sidewalk:segregated" = 'yes'
        OR
        "sidewalk:right:segregated" = 'yes'
    )
)