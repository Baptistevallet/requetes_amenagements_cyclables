(	-- Soit
	(	-- une route explicitement à double sens
		(
			"oneway" LIKE 'no' OR oneway IS NULL
		)
		AND
		(
			"junction" IS NULL
			OR
			"junction" NOT IN ('roundabout','circular')
		)
	)
	AND --et qui a
	(	-- soit une bande cyclable classique
		"cycleway" = 'lane'
		OR
		(	-- soit une bande cyclable à droite et à gauche quelque soit le sens
			"cycleway:right" = 'lane'
			AND "cycleway:left" = 'lane'
		)
		OR	-- soit une bande cyclable à droite et à gauche quelque soit le sens
		"cycleway:both" = 'lane'
	)
	AND -- et qui ne comporte q'une seule voie
	(
	    "lanes" = '1'
	)
)