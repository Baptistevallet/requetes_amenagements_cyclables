-- Sont comptabilisés uniquement les voies limitees à 30 et pas les zones 30
-- Seul le sens voiture est pris en compte

"maxspeed" LIKE '30'
AND
(
	( "zone:maxspeed" IS NULL OR "zone:maxspeed" NOT LIKE 'FR:30')
	AND
	( "source:maxspeed" IS NULL OR "source:maxspeed" NOT LIKE 'FR:zone30')
)
AND "oneway" IN ('yes','-1')
