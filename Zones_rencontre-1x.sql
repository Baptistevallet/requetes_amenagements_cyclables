-- Seul le sens voiture est pris en compte
--Une voie en sens unique
(
"oneway" IN ('yes','-1')
)

AND

	--soit une voie de type living street
	(
	"highway" LIKE 'living_street'

	OR
	
	--soit une voie avec le tag zone:maxspeed ou source:maxspeed
		( 
		"maxspeed"='20'
		AND
			(
			"zone:maxspeed"='FR:20'
			OR
			"source:maxspeed"='FR:zone20'
			)
		)
	)

