-- Une piste cyclable à double sens d'un seul côté de la route (:left/right), rattachée à une route (track) qui peut être à sens unique ou pas :
	(	-- ou la piste peut être à droite
		"cycleway:right" IN ('track','opposite_track')
		AND
		"cycleway:right:oneway" LIKE 'no'
	)
