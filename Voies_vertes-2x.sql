(
	(
		"oneway" IS NULL
		OR
		"oneway" LIKE 'no'
	)
	AND
	(
		"oneway:bicycle" IS NULL
		OR
		"oneway:bicycle" LIKE 'no'
	)
)
AND
(
	(
		"highway" = 'path'
		AND
		"bicycle" = 'designated'
		AND
		"foot" = 'designated'
	)
)
