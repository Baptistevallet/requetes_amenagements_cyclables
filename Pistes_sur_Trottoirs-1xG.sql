
( --modélisation avec un way séparé
    "segregated" LIKE 'yes'
    AND
    "highway" LIKE 'footway'
    AND
    "footway" LIKE 'sidewalk'
    AND
    "bicycle" IN ('yes', 'designated', 'official')
    AND
    (
        "oneway" = '-1'
        OR
        "oneway:bicycle" = '-1'
    )
)
OR
(  --modélisation sur la voirie
    (
        "sidewalk" = 'left'
        OR
        "sidewalk" = 'both'
    )
    AND
    (
        "sidewalk:left:bicycle" IN ('yes', 'designated', 'official')
        AND
        (
           "sidewalk:right:bicycle" IS NULL
           OR
           "sidewalk:right:bicycle" = 'no'
        )
    )
    AND
    (
        "sidewalk:segregated" = 'yes'
        OR
        "sidewalk:left:segregated" = 'yes'
    )
)