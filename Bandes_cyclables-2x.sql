(	-- Soit
	(	-- une route à double sens
		(
			"oneway" IS NULL
			OR
			"oneway" LIKE 'no'
		)
		AND
		(
			"junction" IS NULL
			OR
			"junction" NOT IN ('roundabout','circular')
		)
	)
	AND --et qui a
	(	-- soit une bande cyclable classique
		"cycleway" IN ('lane' , 'opposite_lane')
		OR
		(	-- soit une bande cyclable à droite et à gauche quelque soit le sens
			"cycleway:right" IN ('lane','opposite_lane')
			AND "cycleway:left" IN ('lane','opposite_lane')
		)
		OR	-- soit une bande cyclable à droite et à gauche quelque soit le sens
		"cycleway:both" IN ('lane','opposite_lane')
	)
	AND
	( --et qui ne comporte pas une seul voie sinon c'est un chaucidou
	    "lanes" IS NULL
	    OR
	    "lanes" NOT IN ('1')
	)
)

