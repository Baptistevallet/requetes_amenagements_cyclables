--Soit
	(	-- une route à sens unique ou non, quand la bande de droite est à double sens
		"cycleway:right" IN ('lane','opposite_lane')
		AND
		"cycleway:right:oneway" LIKE 'no'
	)
